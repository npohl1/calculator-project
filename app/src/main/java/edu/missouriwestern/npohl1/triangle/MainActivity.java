package edu.missouriwestern.npohl1.triangle;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText sideOneBox;
    EditText sideTwoBox;
    EditText sideThreeBox;
    TextView resultBox;
    Button calculateBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



                sideOneBox = (EditText) findViewById(R.id.sideOneEditText);
                sideTwoBox = (EditText) findViewById(R.id.sideTwoEditText);
                sideThreeBox = (EditText) findViewById(R.id.sideThreeEditText);
                resultBox = (TextView) findViewById(R.id.calculateEditText);
                Button calculateBtn = (Button) findViewById(R.id.calculateBtn);

                calculateBtn.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View v){
                        double sideOneValue = Double.parseDouble(sideOneBox.getText().toString());
                        double sideTwoValue = Double.parseDouble(sideTwoBox.getText().toString());
                        double sideThreeValue = Double.parseDouble(sideThreeBox.getText().toString());

                        double result = sideOneValue + sideTwoValue + sideThreeValue;

                        resultBox.setText(Double.toString(result));

                    }
                });
            }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
